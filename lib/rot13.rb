class String
  def rot13
    alphabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p',
      'q','r','s','t','u','v','w','x','y','z']

    self.each_char.with_index(0) do |char, index|
      # Ignore non alphabet charactors
      next unless char =~ /^[A-z]+$/
      # Work out where in the array the current char is
      position = alphabet.each_index.select { |i| alphabet[i] == char }.first
      # Replace the current char with one 13 places across, wrapping around to the start if over 26
      self[index] = alphabet[(position+13) % 26]
    end
    return self
  end
end
