Gem::Specification.new do |s|
  s.name        = 'rot13'
  s.version     = '0.1.0'
  s.date        = '2017-07-12'
  s.summary     = "rot13"
  s.description = "Moved every char in a string 13 places"
  s.authors     = ["Luke Picciau"]
  s.email       = 'luke.b.picciau@gmail.com'
  s.files       = ["lib/rot13.rb"]
  s.homepage    =
    'https://gitlab.com/Qwertie/rot13'
  s.license       = 'MIT'
end
